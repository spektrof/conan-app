from conans import ConanFile, CMake, tools

class AppConan(ConanFile):
    name = "App"
    version = "0.1"
    author = "Peter Lukacs lukacs.peter19@gmail.com"
    url = "https://gitlab.com/spektrof/conan-app"
    description = "App which depends on Foo library"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    requires = ["Foo/0.1@spektrof+conan-foo/beta"]

    def configure(self):
        if self.options.shared:
            self.options.fPIC=False

    def source(self):
        self.run("git clone https://gitlab.com/spektrof/conan-app.git")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="conan-app")
        cmake.build()

    def package(self):
        self.copy("*", dst="bin", src="bin")

    def package_info(self):
        self.cpp_info.bindirs = ["bin"]